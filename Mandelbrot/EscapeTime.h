#ifndef ESCAPE_TIME
#define ESCAPE_TIME

#include <cstdint>
static class EscapeTime
{
public:

	static uint32_t point(double real, double imaginary);

	static float iterations_percent(double real, double imaginary);

private:

	static unsigned int _MAX_ITERATIONS;

	static double _SQUARED_NORM;

	static unsigned int iterations(double real, double imaginary);
};
#endif

