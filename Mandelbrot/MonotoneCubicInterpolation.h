#ifndef MONOTONE_CUBIC_INTERPOLATION_H
#define MONOTONE_CUBIC_INTERPOLATION_H

#include <vector>
#include <cmath>

void MCI_create_gradient(
	std::vector<uint32_t>& palette,
	unsigned char* &control,
	int control_size
);

void MCI_compute_slopes_Wikipedia(
	std::vector<double>& slopes, 
	unsigned char* &control, 
	int control_size, 
	int RGB_offset, 
	size_t buffer_size
);

void MCI_compute_slopes_M_Steffen(
	std::vector<double>& y_i,
	unsigned char*& control,
	int control_size,
	int RGB_offset,
	size_t buffer_size
);

unsigned char evaluate(
	double diff, 
	unsigned char point_1, 
	unsigned char point_2, 
	double point_distance, 
	int slopes_index, 
	std::vector<double>& slopes
);

// Basis fucntions
double MCI_h00(double t);
double MCI_h01(double t);
double MCI_h10(double t);
double MCI_h11(double t);




#endif
