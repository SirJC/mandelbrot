#include <iostream>
#include "Display.h"


using namespace std;


int main(char** argv, int argc) {

	Display* display = Display::GetInstance(1280, 720, "Mandelbrot");
	while (display->update()) {
		display->show();
	}
	display->exit();

	return 0;
}