#ifndef PIXEL_PAINTER_H
#define PIXEL_PAINTER_H

#include <iostream>
#include <fstream>
#include <vector>

#include "EscapeTime.h"

class PixelPainter
{
public:
    std::string name;

    PixelPainter(std::string const& name, std::string const& path);

    void updatePixels(std::vector<uint32_t>& pixels, int width, int height, double r, double z, double r_width, double z_widht);

    void showPalette(std::vector<uint32_t>& pixels, int width, int height);

    void showPaletteDiagrams(std::vector<uint32_t>& pixels, int width, int height);

protected:

    static const char* _EXT;

    static const uint32_t _BUFFER_SIZE = 0x00000001 << 10;

#ifdef _WIN32
    static const char _SEP = '\\';
#else
    static const char _SEP = '/';
#endif


    std::vector<std::uint32_t> palette;

    int loadControlPoints(std::string const& file_name, unsigned char* &control);

    void createGradient(unsigned char* control, int size);



};
#endif

