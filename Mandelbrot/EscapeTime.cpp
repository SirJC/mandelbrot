#include "EscapeTime.h"

unsigned int EscapeTime::_MAX_ITERATIONS = 100;

double EscapeTime::_SQUARED_NORM = 1 << 8;


uint32_t EscapeTime::point(double real, double imaginary)
{
	uint32_t ret_val;
	unsigned int iter = iterations(real, imaginary);
	unsigned char value = 255.0 * (_MAX_ITERATIONS - iter) / _MAX_ITERATIONS;
	ret_val =  (255 >> 24) | (value << 16) | (value << 8) | value;

	return ret_val;
}

float EscapeTime::iterations_percent(double real, double imaginary)
{
	return 1.0 * iterations(real, imaginary) / _MAX_ITERATIONS;
}

unsigned int EscapeTime::iterations(double real, double imaginary)
{
	double r, z, r2, z2;
	r = z = r2 = z2 = 0;

	unsigned int iter = 0;
	while (iter < _MAX_ITERATIONS && (r2 + z2 < _SQUARED_NORM)) {
		z = (r + r) * z + imaginary;
		r = r2 - z2 + real;
		r2 = r * r;
		z2 = z * z;
		

		++iter;
	}
	return iter;
}
