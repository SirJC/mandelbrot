#ifndef DISPLAY_H
#define DISPLAY_H

#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <mutex>
#include <iostream>
#include <vector>

#include "PixelPainter.h"

/// Singleton class
class Display
{

private:
	static Display* _instance;
	static std::mutex _mutex;

    static const int FPS = 30;
    static const int FRAME_TIME = 1000 / FPS;
    static const float _SPEED; // Lateral speed in width percentage
    static const float _ZOOM_SPEED; // Ratio in whiche width and height 
                                    // increase (decrease) when zooming

protected:

    // Screen dimensions
    int _WIDTH;
    int _HEIGHT;

    // To control FPS
    uint32_t pastFrame;

    // Camera center position
    double r;
    double z;

    // Target position
    double r_target;
    double z_target;

    // Distance from left most to right most real value
    double r_width;
    // Distance from top most to bottom most imaginary value
    double z_height;

    // SDL components
    SDL_Renderer* renderer;
    SDL_Window* window;
    SDL_Texture* texture;

    // pixels buffer
    std::vector<uint32_t> pixels;
    // marks wheter pixels should update from zoom
    bool loaded;
    // Painter
    PixelPainter painter;


	Display(int w, int h, const char* title);
	~Display();

public:
    

    /**
     * Singletons should not be cloneable.
     */
    Display(Display& other) = delete;
    /**
     * Singletons should not be assignable.
     */
    void operator=(const Display&) = delete;
    /**
     * This is the static method that controls the access to the singleton
     * instance. On the first run, it creates a singleton object and places it
     * into the static field. On subsequent runs, it returns the client existing
     * object stored in the static field.
     */

    static Display* GetInstance(int w, int h, const char* title);

    bool update();

    void show();

    void exit();

protected:

    void init(const char* title, bool cursor);

    void full_reload();

    void logError(std::string const& error_code, std::ostream& out = std::cerr);


};
#endif

