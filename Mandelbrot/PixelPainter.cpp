#include "PixelPainter.h"
#include "MonotoneCubicInterpolation.h"

#include <algorithm>

const char* PixelPainter::_EXT = ".txt";





PixelPainter::PixelPainter(std::string const& _name, std::string const& path) :
	name(_name)
{
	unsigned char* control = nullptr;
	int control_size = loadControlPoints(path + _SEP + name + _EXT, control);

	if (control == nullptr)
		throw std::domain_error("Failed when loading control points");

	createGradient(control, control_size);

	delete[] control;
}

void PixelPainter::updatePixels(std::vector<uint32_t>& pixels, int width, int height, double r, double z, double r_width, double z_width)
{
	for (int x = 0; x < width; ++x) {
		for (int y = 0; y < height; ++y) {
			float percent = EscapeTime::iterations_percent(
				r - r_width / 2 + x * r_width / width,
				z + z_width / 2 - y * z_width / height
			);
			pixels[width * y + x] = palette[(palette.size() - 1) * percent];
		}
	}
}

void PixelPainter::showPalette(std::vector<uint32_t>& pixels, int width, int height)
{
	for (int x = 0; x < width; ++x) {
		for (int y = 0; y < height; ++y) {
			if (x >= palette.size()) pixels[width * y + x] = 0xff000000;
			else
				pixels[width * y + x] = palette[x];
		}
	}
}

void PixelPainter::showPaletteDiagrams(std::vector<uint32_t>& pixels, int width, int height)
{
	int _255 = height * 0.1 - 20;

	// Black screen
	for (auto pixel = pixels.begin(); pixel != pixels.end(); ++pixel) {
		*pixel = 0xff000000;
	}
	// Draw a white line for top 255
	for (int i = 0; i < width; ++i) {
		pixels[width * (_255 - 1) + i] = 0xffffffff;
		pixels[width * (_255) + i]     = 0xffffffff;
	}

	// Draw RGB lines
	for (int i = 0; i < width - 1; ++i) {
		if (i >= palette.size()) break;

		unsigned int R, G, B;
		uint32_t R_val, G_val, B_val;
		uint32_t test = palette[i];
		float factor = height / 255.0;
		R_val = ((palette[i] & 0x00ff0000) >> 16);
		G_val = ((palette[i] & 0x0000ff00) >> 8);
		B_val = (palette[i] & 0x000000ff);
		R = height - 1 - std::min((unsigned int)height - 1, (unsigned int)(R_val*factor));
		G = height - 1 - std::min((unsigned int)height - 1, (unsigned int)(G_val*factor));
		B = height - 1 - std::min((unsigned int)height - 1, (unsigned int)(B_val*factor));
		pixels[i + width * R] |= 0xffff0000;
		pixels[i + width * G] |= 0xff00ff00;
		pixels[i + width * B] |= 0xff0000ff;
	}
}

int PixelPainter::loadControlPoints(std::string const& file_name, unsigned char* &control)
{
	int size = -1;
	std::ifstream input(file_name);
	if (input.is_open()) {
		input >> size;
		int R, G, B;
		control = new (unsigned char[3 * size]);
		for (int i = 0; i < 3 * size; i += 3) {
			input >> R >> G >> B;
			input.ignore(1000, '\n');
			control[i]     = R;
			control[i + 1] = G;
			control[i + 2] = B;
		}
	}
	else 
		control = nullptr;
	return size;
}

void PixelPainter::createGradient(unsigned char* control, int size)
{
	palette.resize(_BUFFER_SIZE);
	MCI_create_gradient(palette, control, size);
}
