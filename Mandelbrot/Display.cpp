#include "Display.h"
#include <iostream>
#include "EscapeTime.h"

const float Display::_SPEED = 0.9;
const float Display::_ZOOM_SPEED = 0.5;



#ifdef FIXED_TARGET
Display::Display(int w, int h, const char* title, double _r_target, double _z_target) :
    _WIDTH(w),
    _HEIGHT(h),
    loaded(false),
    painter("Wikipedia", "Gradients"),
    r_target(_r_target),
    z_target(_z_target)
{
    init(title, true);
}
#else
Display::Display(int w, int h, const char* title) :
    _WIDTH(w),
    _HEIGHT(h),
    loaded(false),
    painter("Wikipedia", "Gradients")
{
    init(title, true);
}

#endif

Display::~Display()
{
}

Display* Display::_instance = nullptr;
std::mutex Display::_mutex;

Display* Display::GetInstance(int w, int h, const char* title)
{
    if (_instance == nullptr)
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if (_instance == nullptr)
        {
            _instance = new Display(w, h, title);
        }
    }
    return _instance;
}

bool Display::update()
{
    SDL_Event event;
    unsigned char key_mask = 0x00;
    bool zooming = false;
    /**
     * SDLK_UP = 0x08
     * SDLK_DOWN = 0x04
     * SDLK_LEFT = 0x02
     * SDLK_RIGHT = 0x01
     */
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            return false;
            break;
        case SDL_MOUSEWHEEL:
            // If we are already moving sideways don't zoom
            if (key_mask & 0x0f)
                continue;
            zooming = true;
            // Increase or decrease zoom
            r_width *= event.wheel.y > 0 ? _ZOOM_SPEED : 1 / _ZOOM_SPEED;
            z_height *= event.wheel.y > 0 ? _ZOOM_SPEED : 1 / _ZOOM_SPEED;
#ifdef FIXED_TARGET
            // nothing here
#else
            // Get zoom and mouse position
            int x, y;
            SDL_GetMouseState(&x, &y);
            r_target = r - r_width / 2 + 1.0 * x / _WIDTH * r_width;
            z_target = z + z_height / 2 - 1.0 * y / _HEIGHT * z_height;

            /*
            double rt2 = (std::abs(r_target - z) / r_width);
            double zt2 = (std::abs(z_target - z) / z_height);
            r = rt2 * r + (1 - rt2) * r_target;
            z = zt2 * z + (1 - zt2) * z_target;
            */
            r = r_target;
            z = z_target;
#endif

            loaded = 0;
            break;
        case SDL_KEYDOWN:
            // If we are zooming don't move
            if (zooming)
                continue;

            if      (event.key.keysym.scancode == SDL_SCANCODE_UP) {
                key_mask |= 0x08;
            }
            else if (event.key.keysym.scancode == SDL_SCANCODE_DOWN) {
                key_mask |= 0x04;
            }
            else if (event.key.keysym.scancode == SDL_SCANCODE_LEFT) {
                key_mask |= 0x02;
            }
            else if (event.key.keysym.scancode == SDL_SCANCODE_RIGHT) {
                key_mask |= 0x01;
            }
            //DEBUG
            else if (event.key.keysym.scancode == SDL_SCANCODE_A) {
                key_mask |= 0x10;
            }
            else if (event.key.keysym.scancode == SDL_SCANCODE_S) {
                key_mask |= 0x11;
            }
            //END DEBUG
            else if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
                return false;
            }

        }

    }
    if (key_mask == 0x10) {
        painter.showPalette(pixels, _WIDTH, _HEIGHT);
        show();
        loaded = 1;
    }
    if (key_mask == 0x11) {
        painter.showPaletteDiagrams(pixels, _WIDTH, _HEIGHT);
        show();
        loaded = 1;
    }
    // Process movement
    r_target += (key_mask & 0x01 ? _SPEED * r_width : 0) - (key_mask & 0x02 ? _SPEED * r_width : 0);
    z_target += (key_mask & 0x01 ? _SPEED * r_width : 0) - (key_mask & 0x02 ? _SPEED * r_width : 0);
    if (!loaded) {
        full_reload();
    }
    return true;
}

void Display::show()
{
    // First check if we should show the changes depending on FPS counter
    uint32_t current = SDL_GetTicks();
    unsigned int delta_time = current - pastFrame;
    //std::cout << delta_time << '\n';
    // Don't delay as I think it blocks the execution, instead return ton continue with the updates
    // This makes FPS less stable, but whatever, not interested in that
    if (delta_time < FRAME_TIME) return;
    pastFrame = current;

    // update the screen
    SDL_RenderPresent(renderer);
    SDL_UpdateTexture(texture, NULL, &pixels[0], _WIDTH * sizeof(Uint32));
    SDL_RenderCopy(renderer, texture, NULL, NULL);

}

void Display::exit()
{
    // Clear everything
    pixels.clear();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyTexture(texture);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Display::init(const char* title, bool cursor)
{
    // Initialize SDL video
    if (SDL_Init(SDL_INIT_VIDEO) != 0) logError("SDL_Init");

    // Create the Window and notify any error
    window = SDL_CreateWindow(
        title,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        _WIDTH,
        _HEIGHT,
        0
    );
    if (window == nullptr) logError("SDL_CreateWindow");

    // Create the Renderer and notify any error
    renderer = SDL_CreateRenderer(
        window,
        -1,
        SDL_RENDERER_ACCELERATED
    );
    if (renderer == nullptr) logError("SDL_CreateRenderer");

    // Create the Texture and notify any error
    texture = SDL_CreateTexture(
        renderer, 
        SDL_PIXELFORMAT_ARGB8888, 
        SDL_TEXTUREACCESS_STREAMING, 
        _WIDTH, 
        _HEIGHT
    );
    if (texture == nullptr) logError("SDL_CreateTexture");

    SDL_ShowCursor(cursor);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");

    // Create the pixels array
    pixels = std::vector<Uint32>(_WIDTH * _HEIGHT);

    // Initial posiition
    // TODO: Shouldn't be hardcoded
    r = -1;
    r_width = 4;
    z_height = r_width * _HEIGHT / _WIDTH;
    z = 0;
}

void Display::full_reload()
{
    // Recaculate every pixel
    painter.updatePixels(pixels, _WIDTH, _HEIGHT, r, z, r_width, z_height);
    loaded = 1;
}

void Display::logError(std::string const& error_code, std::ostream& out)
{
    out << error_code << ": " << SDL_GetError() << '\n';
}

