#include "MonotoneCubicInterpolation.h"

#include <iostream>
#include <iomanip>
#include <assert.h>
#include <algorithm>
// Auxiliar function

// First, we know control has memory reserved for 3 * control_size unsigned char. 
void MCI_create_gradient(std::vector<uint32_t>& palette, unsigned char* &control, int control_size)
{
	// First compute the slopes for monotonic cubic interpolation
	std::vector<double> R_slopes, G_slopes, B_slopes;

	MCI_compute_slopes_Wikipedia(R_slopes, control, control_size, 0, palette.capacity());
	MCI_compute_slopes_Wikipedia(G_slopes, control, control_size, 1, palette.capacity());
	MCI_compute_slopes_Wikipedia(B_slopes, control, control_size, 2, palette.capacity());

	// Cubic interpolation
	int k = 0;
	size_t buffer_size = palette.size();
	double point_distance = 1.0 * (buffer_size-1) / (control_size-1);
	for (size_t i = 0; i < buffer_size - 1; ++i) {
		
		double diff = i - k * point_distance;
		assert(k < 15);
		palette[i] =
			(0xff << 24) |
			(evaluate(diff, control[3 * k]    , control[3 * k + 3], point_distance, k, R_slopes) << 16) |
			(evaluate(diff, control[3 * k + 1], control[3 * k + 4], point_distance, k, G_slopes) << 8) |
			(evaluate(diff, control[3 * k + 2], control[3 * k + 5], point_distance, k, B_slopes));
		if (point_distance * (k + 1) <= i)
			++k;
	}
	// make last color black
	palette[buffer_size - 1] = 0x00000000;

}

void MCI_compute_slopes_Wikipedia(std::vector<double>& slopes, unsigned char* &control, int control_size, int RGB_offset, size_t buffer_size)
{
	double point_distance = 1.0 * (buffer_size-1) / (control_size-1); // We are assuming all points are at the same distance

	// 1.) Compute the slopes of the secant lines
	std::vector<double> secants = std::vector<double>(control_size - 1);
	for (int i = 0; i < control_size - 1; ++i) {
		secants[i] = (control[3*(i+1) + RGB_offset] - control[3*i + RGB_offset]) / point_distance;
	}
	
	// 2.) Initialize the tangents at every interior data point as the average of the secants
	slopes.clear();
	slopes.resize(control_size);
	slopes[0] = secants[0];
	for (int i = 1; i < control_size - 1; ++i) {
		slopes[i] = 
			(secants[i]*secants[i-1] > 0) ?
			(secants[i] + secants[i - 1]) / 2
			: 0;
	}
	slopes[control_size - 1] = secants[control_size - 2];

	/** 3.) If two consecutive points control[3*k+RGB_offset] control[3*(k+1)+RGB_offset] have the same value set 
	 * slopes_k and slopes_k+1 to 0. Ignore steps 4 and 5 for those	k.
	 */
	std::vector<int> to_skip;
	for (int i = 0; i < control_size - 1; ++i) {
		if (secants[i] == 0) {
			slopes[i] = slopes[i + 1] = 0;
			to_skip.push_back(i);
		}
	}

	// Steps 4 and 5 explained in Wikipedia: https://en.wikipedia.org/wiki/Monotone_cubic_interpolation
	auto it = to_skip.begin();
	for (int i = 0; i < control_size - 1; ++i) {

		if (it != to_skip.end() && i == *it) {
			++it;
			continue;
		}

		double alpha = slopes[i]     / secants[i];
		double beta =  slopes[i + 1] / secants[i];

		if (alpha < 0) slopes[i] = 0;
		if (beta < 0)  slopes[i + 1] = 0;

		double squared_alpha_beta = alpha * alpha + beta * beta;
		if (squared_alpha_beta > 9) {
			double tau = 3 / sqrt(squared_alpha_beta);

			slopes[i]     *= tau * slopes[i];
			slopes[i + 1] *= tau * slopes[i + 1];
		}
	}
}

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

// Data set must have at least 3 values, otherwise, it is undefined behaviour outside debug or error on forbidden memory
void MCI_compute_slopes_M_Steffen(std::vector<double>& y_i, unsigned char*& control, int control_size, int RGB_offset, size_t buffer_size)
{
	assert(control_size > 2);

	// We will follow the notation on M.Steffen's paper "A simple method for monotonic interpolation in one dimension".
	double h_i = 1.0 * (buffer_size-1) / (control_size-1); // We are assuming all points are at the same distance

	std::vector<double> s_i(control_size - 1); // The slope of the secants through points (x_i, y_i; x_i+1, y_i+1)
	for (int i = 0; i < control_size - 1; ++i) {
		s_i[i] = 1.0 * (control[3 * i + RGB_offset] - control[3 * (i + 1) + RGB_offset]) / h_i;
	}

	std::vector<double> p_i(control_size - 2); // For equidistant data sample, this is the same as the slope of the
	// secant through (x_i-1, y_i-1; x_i+1, y_i+1)
	// Note that as there is one less p_i than s_i
	for (int i = 0; i < control_size - 2; ++i) {
		// p_i[i] = (s_i[i] * h_i + s_i[i + 1] * h_i) / (h_i + h_i); this is the actual expression. As it is equidistant
		p_i[i] = (s_i[i] + s_i[i + 1]) / 2;
	}

	// Now we calculate y_i under M.Steffen condition to assure monotonic interpolation
	y_i.clear();
	y_i.resize(control_size);
	// for i = 0 and i = control_size - 1 special conditions will be calculated later
	for (int i = 1; i < control_size - 1; ++i) {
		// y_i[i] = (sgn(s_i[i - 1]) + sgn(s_i[i]) * std::min(abs(s_i[i - 1]), std::min(abs(s_i[i]), 0.5 * p_i[i - 1])));
		// Paper calculates it like this
		if (s_i[i] * s_i[i - 1] <= 0) {
			y_i[i] = 0;
		}
		else if (abs(p_i[i - 1]) > 2 * abs(s_i[i]) || abs(p_i[i-1]) > 2 * abs(s_i[i-1])) {
			y_i[i] = 2 * sgn(s_i[i]) * std::min(s_i[i], s_i[i - 1]);
		}
		else {
			y_i[i] = p_i[i - 1];
		}
	}

	// for the boundires there are more sophisticated methods, but we will just set it to the secants
	// as it preserves already monotonicity
	y_i[0] = s_i[0];
	y_i[control_size - 1] = s_i[control_size - 2];

}

// TODO: optimize, but please, dont break it :)
unsigned char evaluate(double diff, unsigned char point_1, unsigned char point_2, double point_distance, int slopes_index, std::vector<double>& slopes)
{
	double s_i = (point_2 - point_1) / point_distance;
	double a_i = (slopes[slopes_index] + slopes[slopes_index + 1] - 2*s_i) / (point_distance * point_distance);
	double b_i = (3 * s_i - 2 * slopes[slopes_index] - slopes[slopes_index + 1]) / point_distance;
	double c_i = slopes[slopes_index];
	double d_i = point_1;
	unsigned int ret_val = a_i * (diff * diff * diff) +
		b_i * (diff * diff) +
		c_i * diff + d_i;
	return ret_val;
		
}

double MCI_h00(double t)
{
	return 2* t * t * t - 3 * t * t + 1;
}

double MCI_h01(double t)
{
	return t * t * t - 2 * t * t + t;
}

double MCI_h10(double t)
{
	return -2 * t * t * t + 3 * t * t;
}

double MCI_h11(double t)
{
	return t * t * t - t * t;
}
